"""The BycicleAnalysis Class calls and executes class methods.
    The final outputs are the requested correlation matrix
    and the plots.
    """
from urllib.request import urlopen
from zipfile import ZipFile
from datetime import datetime, timedelta
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt


class BycicleAnalysis:
    """
    Class that controls all class methods and finally
    delivers the requested information.
    It analyses bike rental data, outputting four different charts.

    Attributes
    ----------------
    url: str
        The url for the requested zip file
    file_path: str
        The path where the zip-file is to be saved
    agg_patg: str
        The path where the hour-csv-file is found
    cols: list
        The columns included in the correlation matrix

    Methods
    ----------------
    __init__: Init method
        Class constructor to inizialize the attributes of the class.
    download_zip: Class method 1
        Downloads the requested zip file and saves it in a downloads folder.
    hourly_agg: Class method 2
        Reads a csv file and returns the pandas dataframe
        with hourly aggregations.
    correlation_matrix: Class method 3
        Computes correlation matrix for given columns.
    plot_weeks: Class method 4
        Receives a date as input from user and plots''
        rental count for each hour in that week, starting with Monday.
    avg_total_rentals: Class method 5
        Plots the average total rentals by month of the year.
    forecast: Class method 6
        Plots expected number of rentals per hour in a week.
    """

    # init method
    def __init__(self, url: str, file_path: str, agg_path: str, cols: list):
        """
        Class constructor to inizialize the attributes of the class.

        Parameters
        ----------------
        url: str
            The url for the requested zip file
        file_path: str
            The path where the zip-file is to be saved
        agg_path: str
            The path where the hour-csv-file is found
        cols: list
            The columns included in the correlation matrix
        """

        self.url = url
        self.file_path = file_path
        self.agg_path = agg_path
        self.cols = cols

    # method 1 --> download zip file
    def download_zip(self):
        """
        Downloads the requested zip file and saves it in a downloads folder.

        Parameters
        ----------------
        None

        Raises
        ----------------
        Exception
            If an error occurs while downloading the
            zip file and saving it into the downloads-file.
        """

        zipurl = self.url
        save_path = self.file_path

        try:
            # Download the file from the URL
            readzip = urlopen(zipurl)
            # Create a new file where to save the zip-file
            tempzip = open(save_path, "wb")
            # Write the contents of the downloaded file into the new file
            tempzip.write(readzip.read())
            # Close the new file
            tempzip.close()
            # Re-open the new file
            newfile = ZipFile(save_path)
            # Extract its contents into a downloads folder
            newfile.extractall(path='downloads')
        except Exception:
            raise Exception("Error 404") from Exception
        else:
            # Close the zip-file instance
            newfile.close()

    # method 2 --> read hourly_agg into dataframe
    def hourly_agg(self):
        """
        Reads a csv file and returns the pandas
        dataframe with hourly aggregations.

        Parameter
        --------------
        None

        Raises
        ----------------
        Exception
            If an error occurs while reading the
            csv file and creating a dataframe.

        Returns
        --------------
        dataset: pandas dataframe
        """

        try:
            dataframe = pd.read_csv(self.agg_path)
            dataframe.dteday = pd.to_datetime(dataframe.dteday)
            dataframe.index = (dataframe.dteday +
                               pd.to_timedelta(dataframe.hr, 'h'))
        except Exception:
            raise Exception("File not found.") from Exception
        else:
            return dataframe

    # method 3 --> plot correlation matrix of given columns
    def correlation_matrix(self):
        """
        Computes correlation matrix for given columns.

        Parameter
        --------------
        None

        Raises
        ----------------
        Exception
            If an error occurs when creating the correlation matrix.

        Returns
        --------------
        corr_matrix: pandas dataframe
        """

        dataframe = self.hourly_agg()

        try:
            corr_matrix = dataframe[self.cols].corr()
        except Exception:
            raise Exception("The matrix could not be created.") from Exception
        else:
            plt.subplots(figsize=(8, 6))
            sns.heatmap(corr_matrix)
            plt.title("Correlation matrix")
            plt.tight_layout()
            plt.show()
            return corr_matrix

    # method 4 --> plots the rental count for each hour during a week.
    def plot_weeks(self):
        """
        Receives a date as input from user and plots
        rental count for each hour in that week, starting with Monday.

        Parameter
        --------------
        None

        Raises
        ----------------
        ValueError
            If the date input is not in the range of the
            dataset (2011-01-01 - 2012-12-31).
        Exception
            If an error occurs when plotting the linechart.

        Returns
        --------------
        plot: matplotlib.pyplot linechart
            Plots rental count for each hour
        """

        dataframe = self.hourly_agg()
        print("Select a date between 2011-01-03 - 2012-12-30:")
        print("Please enter a year (e.g. 2011): ")
        year = str(input())
        year.lstrip("0")
        year = int(year)
        print("Please enter a month (e.g. 8 or 08): ")
        month = str(input())
        month.lstrip("0")
        month = int(month)
        print("Please enter a day (e.g. 9 or 09): ")
        day = str(input())
        day.lstrip("0")
        day = int(day)
        search_date = datetime(year, month, day)
        start_date = search_date - timedelta(days=search_date.weekday())
        end_date = start_date + timedelta(days=7)

        try:
            if ((start_date >= dataframe.index.min()) and
                    (end_date < dataframe.index.max())):
                dataf = dataframe[start_date:end_date]
                plt.figure(figsize=(11, 8))
                plt.title("Rentals in week starting on " + str(start_date))
                plt.xlabel("Hours in period")
                plt.ylabel("Number of bike rentals")
                plt.plot(dataf.index, dataf['cnt'])
                plt.show()
            else:
                raise ValueError("The date is not the desired range.")
        except Exception:
            raise Exception("An error occured.") from Exception

    def avg_total_rentals(self):
        """
        Plots the average total rentals by month of the year.

        Parameter
        --------------
        None

        Raises
        ----------------
        Exception
            If an error occurs creating a new grouped dataframe.

        Returns
        --------------
        plot: matplotlib.pyplot barchart
            Plots the average total rentals by month of the year
        """

        dataframe = self.hourly_agg()
        try:
            avg_dataframe = dataframe.groupby(["yr", "mnth"])['cnt'].sum()
            avg_dataframe = avg_dataframe.groupby(["mnth"]).mean()
        except Exception:
            message = "The dataframe could not be created."
            raise Exception(message) from Exception
        else:
            y_pos = np.arange(1, 13)
            values = avg_dataframe
            plt.figure(figsize=(10, 8))
            plt.title("Average total rentals by month of the year ")
            plt.bar(y_pos, values, align='center', alpha=0.5)
            plt.xticks(y_pos, y_pos)
            plt.ylabel("Average per Month")
            plt.xlabel("Months")
            plt.show()

    def forecast(self):
        """
        Plots the expected number of rentals per hour in a
        week in a given month.

        Parameter
        --------------
        None

        Raises
        ----------------
        Exception
            If an error occurs creating a new grouped dataframe.

        Returns
        --------------
        plot: matplotlib.pyplot linechart with standard deviation
            Plots expected number of rentals per hour in a week including a
            shaded area for the standard deviation.
        """

        dataframe = self.hourly_agg()
        print("Please select a month:")
        selected_month = int(input())
        try:
            dataframe.loc[dataframe['weekday'] == 0, 'weekday'] = 7
            forecast_dataframe = (dataframe[dataframe['mnth'] ==
                                            selected_month])
            forecast_mean = (forecast_dataframe.groupby
                             (["weekday", "hr"]).cnt.agg(['mean', 'std']))
            forecast_mean = (forecast_mean.set_index
                             (np.arange(1, 169)))
        except Exception:
            message = "The dataframe could not be created."
            raise Exception(message) from Exception
        else:
            fig_ure, ax_es = plt.subplots(figsize=(12, 5))
            (ax_es.fill_between(forecast_mean.index,
                                forecast_mean['mean']-forecast_mean['std'],
                                forecast_mean['mean']+forecast_mean['std'],
                                alpha=0.4))
            forecast_mean['mean'].plot(ax=ax_es, c='C3')
            ax_es.set_ylabel("Average number of rentals by hour")
            ax_es.set_xlabel("Aggregated hours per week")
            (ax_es.set_title("Expected number of rentals per hour in a" +
                             " week in month " + str(selected_month)))
            fig_ure.tight_layout()
            plt.show()
