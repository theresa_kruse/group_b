# Group_B

### Mobility Analysis of Bycicle Users

Our team is participating in a study of the mobility of bycicle users. We created a class called BycicleAnalysis, which executes six methods as well as one class constructor in order to initialize the attributes of the class.

Our main goal in this analysis is to get deeper insights on the correlations between different weather conditions and the number of rented bycicles. Also, we want to identify what hours are the busiest in a given week, based on a user given start date.

Additionally, we analysed the average of total rentals for every month of the year, to get an overview of the distribution of rentals over a year. Finally, we evaluated the expected number of rentals per hour in a given month (given by user), including the standard deviation. By plotting the mean and standard deviation of rental numbers, we were able to identify more volatile week-days in a given month.

This analysis may then be used to help bike rentals to adapt better to demand on certain months & days and for certain weather conditions. Based on our analysis bike rental companies can try to minimize their operating costs, by optimizing their stocks and availability. 

### Group Members: 
- Jacob Lind (32614) - 32614@novasbe.pt
- Arne Wawers (44694) - 44694@novasbe.pt
- Moritz Engelsmann (44398) - 44398@novasbe.pt
- Theresa Kruse (43891) - 43891@novasbe.pt

### Task Distribution: 
Our group decided to do all tasks together as they are highly interdependent.  
We now contributed Via different machines to fulfill the requirements.

### Python-Code: 
Our code can be found in bycicle_analysis.py.

### Showcase-Notebook:
Our showcase notebook can be found in showcase_bycicle_analysis.ipynb.

### Anaconda-Environment:
Our anaconda environment can be found in group_b_environment.yml.

### Packages:
Our packages can be found in packages.txt.
